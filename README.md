# Sensor Data Nearshore Pacific Biological Station

__Main author:__  Cole Fields  
__Affiliation:__  Fisheries and Oceans Canada (DFO)   
__Group:__        Marine Spatial Ecology and Analysis   
__Location:__     Institute of Ocean Sciences   
__Contact:__      e-mail: Cole.Fields@dfo-mpo.gc.ca | tel:


- [Objective](#objective)
- [Summary](#summary)
- [Status](#status)
- [Contents](#contents)
- [Methods](#methods)
- [Requirements](#requirements)
- [Caveats](#caveats)
- [Uncertainty](#uncertainty)
- [Acknowledgements](#acknowledgements)
- [References](#references)


## Objective
Data repository for nearshore sensor data for Pacific Biological Station.


## Summary
The end goal is to submit data to [https://data.cioospacific.ca/erddap/index.html](CIOOS Pacific ERDDAP).


## Status
In-development


## Contents
This is the repository for raw and processed sensor data collected by the Pacific Biological Station.


## Data Flow
* Download new measurements from the sensors
* Using the sensor related software, export the measurements to a CSV file
* Upload csv file to [https://www.sensor-data-uploader.com/](Sensor data uploader) application to add a quality control column to the file. The processed file will be emailed to Cole Fields. Click _List Processed Files_ to list the processed files that are available.
* Add pre-processed and processed csv files to repository in appropriate subfolder.
